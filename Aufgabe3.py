#!/usr/bin/env python3

import requests
from requests.auth import HTTPBasicAuth

# Working curl command:
# $ curl 'https://backend.staffbase.com/api/users/import/csv/upload'  -X POST -H 'Authorization: Basic NjM2M2ViZWQ1NTgyNGI0MDhlZTc1MjdiOlg2Z3UkNWJDdjNIXTAre0hzS1d6ZTJTVSxaUiZKcHNqaTY5Vyk2aUhlNkF9dlRbNVNnWCR4XnJVfjU4eDNUVE0=' -H 'Content-Type: multipart/form-data' -F 'csv=@./users_example.csv' -F 'encoding=utf-8' -F 'fieldSeparator=";"'

API_TOKEN = "XXXX"

base_url = "https://app.staffbase.com/api/users/import/csv/update/"
getlatest_url = "https://app.staffbase.com/api/users/import/csv/latest"
postupdate_url = "https://app.staffbase.com/api/users/import/csv/update"
postupload_url = "https://app.staffbase.com/api/users/import/csv/upload"
delete_url = "https://backend.staffbase.com/api/users/import/csv/XXXX"

updateheaders = {'Authorization': f'Basic {API_TOKEN}', 'accept': 'application/x-www-form-urlencoded; charset=utf-8'}
uploadheaders = {'Authorization': f'Basic {API_TOKEN}', 'accept': 'multipart/form-data', 'accept': 'application/json'}
getheaders = {'Authorization': f'Basic {API_TOKEN}', 'Content-Type': 'application/json; charset=utf-8'}
delheaders = {'Authorization': f'Basic {API_TOKEN}'}

# https://developers.staffbase.com/references/avatar-type/#avatar-example
# avatarjson = [
#     {
#         "avatar": {
#             "original": {
#                 "url": "pictures/A000001.png",
#                 "width": 225,
#                 "height": 225,
#                 "size": 4650,
#                 "format": "png",
#                 "mimeType": "image/png",
#                 "created": "2022-11-07T07:32:56.000Z"
#             }
#         }
#     }
# ]

# https://developers.staffbase.com/references/csv-import-mappings/#csv-import-mappings


csv = "users_example.csv"
csvbin = open(csv, 'rb')
# files = {'upload_file': open(csv,'rb')}
data = {
  "csv": csvbin,
  "encoding": "utf-8",
  "recordSeparator": "\n",
}

# Shows the latest update:
getresponse = requests.get(getlatest_url, headers=getheaders)
print(getresponse.content)

# Upload of a new csv:
# with open(csv, 'rb') as f:
# myfile = open("users_example.csv", "rb")
# uploadresponse = requests.post(postupload_url, headers=uploadheaders, data=data)
# print(uploadresponse.content)

# Update of an file
# updateresponse = requests.post(postupdate_url, headers=updateheaders, json=payload)
# print(updateresponse.content)
# delresponse = requests.delete(delete_url, headers=delheaders)
# print(delresponse.content)
