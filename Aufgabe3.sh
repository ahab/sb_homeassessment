#!/usr/bin/env sh

APITOKEN="XXX"

upload_url="https://backend.staffbase.com/api/users/import/csv/upload"
upload_csv="users_example.csv"
update_csv1="users2_example.csv"
update_csv2="users3_example.csv"

getlatest_url="https://app.staffbase.com/api/users/import/csv/latest"

delete_url="https://backend.staffbase.com/api/users/import/csv/${uploadid}"

update_url="https://app.staffbase.com/api/users/import/csv/update"

function initialupload() {
    curl $upload_url -X POST -H "Authorization: Basic ${APITOKEN}" -H 'Content-Type: multipart/form-data' -F "csv=@./${upload_csv}" -F 'encoding=utf-8' -F 'fieldSeparator=";"' -F 'recordSeparator="\n"'
}

function checklatest() {
    curl $getlatest_url -X GET -H "Authorization: Basic ${APITOKEN}" -H 'Content-Type: application/json'
}

function getid() {
    checklatest | cut -d: -f2 | cut -d, -f1 | cut -d\" -f2
}

function deleteall() {
    uploadid="$(checklatest | cut -d: -f2 | cut -d, -f1 | cut -d\" -f2)"
    delete_url="https://backend.staffbase.com/api/users/import/csv/${uploadid}"
    curl $delete_url -X DELETE -H "Authorization: Basic ${APITOKEN}"
}

function addcsv() {
    curl ${update_url} -X POST -H "Authorization: Basic ${APITOKEN}" -H 'Content-Type: application/x-www-form-urlencoded' -d 'mappings=externalID,profile-field:firstName,profile-field:lastName,eMail,profile-field:position,profile-field:department,profile-field:hiredate' -d 'sendMailsNew=false' -d 'sendMailsPending=false' -d 'generateRecoveryCodes=false' -d 'dry=false' # --compressed | json_pp > dry_run.json
}